Vagrant for Hadoop, Spark, Kafka, HBase
==================================

# Introduction

Vagrant project to spin up a single virtual machine running:

* Kafka 0.10.1.0
* Hadoop 2.7.2
* HBase 0.94.24
* Spark 1.6.0

The virtual machine will be running the following services:

* HDFS DataNode + NameNode
* YARN ResourceManager + JobHistoryServer + ProxyServer
* Kafka Broker
* HBase standalone
* Spark services

# Getting Started

1. [Download and install VirtualBox](https://www.virtualbox.org/wiki/Downloads)
2. [Download and install Vagrant](http://www.vagrantup.com/downloads.html).
3. Run ```vagrant up``` to create the VM.
4. Execute ```vagrant ssh``` to login to the VM.

# Web user interfaces

Here are some useful links to navigate to various UI's:

* YARN resource manager:  (http://10.211.55.101:8088)
* Job history:  (http://10.211.55.101:19888/jobhistory/)
* HDFS: (http://10.211.55.101:50070/dfshealth.html)
* Spark history server: (http://10.211.55.101:18080)
* Spark context UI (if a Spark context is running): (http://10.211.55.101:8080)
* HBase UI: (http://10.211.55.101:60010)

# Validating your virtual machine setup

To test out the virtual machine setup, and for examples of how to run
MapReduce, Hive and Spark, head on over to [VALIDATING.md](VALIDATING.md).

# Starting services in the event of a system restart

Currently if you restart your VM then the Hadoop/Spark/Hive services won't be
up (this is something I'll address soon).  In the interim you can run the
following commands to bring them up:

```
$ vagrant ssh
$ sudo -s
$ /vagrant/scripts/start-hadoop.sh
$ /vagrant/scripts/start-services.sh
```

# More advanced setup

If you'd like to learn more about working and optimizing Vagrant then
take a look at [ADVANCED.md](ADVANCED.md).

# For developers

The file [DEVELOP.md](DEVELOP.md) contains some tips for developers.

# Credits

This project is based on the great work carried out at
(https://github.com/alexholmes/vagrant-hadoop-spark-hive).
