#!/bin/sh

export KAFKA_HOME=/usr/local/kafka
export PATH=${KAFKA_HOME}/bin:${KAFKA_HOME}/sbin:${PATH}