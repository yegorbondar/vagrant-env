#!/bin/sh

export MONGODB_HOME=/usr/local/mongodb
export PATH=${MONGODB_HOME}/bin:${MONGODB_HOME}/sbin:${PATH}