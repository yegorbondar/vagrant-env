#!/bin/sh

export ZOOKEEPER_HOME=/usr/local/zookeeper
export PATH=${ZOOKEEPER_HOME}/bin:${ZOOKEEPER_HOME}/sbin:${PATH}