#!/bin/bash

# java
JAVA_ARCHIVE=jdk-8u51-linux-x64.gz

# zookeeper
ZOOKEEPER_PREFIX=/usr/local/zookeeper
ZOOKEEPER_CONF=$ZOOKEEPER_PREFIX/conf
ZOOKEEPER_VERSION=zookeeper-3.4.9
ZOOKEEPER_ARCHIVE=$ZOOKEEPER_VERSION.tar.gz
ZOOKEEPER_MIRROR_DOWNLOAD=http://apache.volia.net/zookeeper/$ZOOKEEPER_VERSION/$ZOOKEEPER_ARCHIVE
ZOOKEEPER_RES_DIR=/vagrant/resources/zookeeper

# kafka
KAFKA_PREFIX=/usr/local/kafka
KAFKA_CONF=$KAFKA_PREFIX/etc/kafka
KAFKA_VERSION=kafka_2.11-0.10.1.0
KAFKA_ARCHIVE=$KAFKA_VERSION.tgz
KAFKA_MIRROR_DOWNLOAD=http://apache.volia.net/kafka/0.10.1.0/$KAFKA_VERSION.tgz
KAFKA_RES_DIR=/vagrant/resources/kafka

# mongodb
MONGODB_PREFIX=/usr/local/mongodb
MONGODB_CONF=$MONGODB_PREFIX/etc/mongodb
MONGODB_VERSION=mongodb-linux-x86_64-3.4.7
MONGODB_ARCHIVE=$MONGODB_VERSION.tgz
MONGODB_MIRROR_DOWNLOAD=https://fastdl.mongodb.org/linux/$MONGODB_VERSION.tgz
MONGODB_RES_DIR=/vagrant/resources/mongodb

# hadoop
HADOOP_PREFIX=/usr/local/hadoop
HADOOP_CONF=$HADOOP_PREFIX/etc/hadoop
HADOOP_VERSION=hadoop-2.7.2
HADOOP_ARCHIVE=$HADOOP_VERSION.tar.gz
HADOOP_MIRROR_DOWNLOAD=http://archive.apache.org/dist/hadoop/core/$HADOOP_VERSION/$HADOOP_ARCHIVE
HADOOP_RES_DIR=/vagrant/resources/hadoop

# hive
HIVE_VERSION=hive-1.2.1
HIVE_ARCHIVE=apache-$HIVE_VERSION-bin.tar.gz
HIVE_MIRROR_DOWNLOAD=http://archive.apache.org/dist/hive/$HIVE_VERSION/$HIVE_ARCHIVE
HIVE_RES_DIR=/vagrant/resources/hive
HIVE_CONF=/usr/local/hive/conf

# spark
SPARK_VERSION=spark-2.1.1
SPARK_ARCHIVE=$SPARK_VERSION-bin-hadoop2.tgz
SPARK_MIRROR_DOWNLOAD=http://archive.apache.org/dist/spark/$SPARK_VERSION/$SPARK_VERSION-bin-hadoop2.6.tgz
SPARK_RES_DIR=/vagrant/resources/spark
SPARK_CONF_DIR=/usr/local/spark/conf

# hbase
HBASE_VERSION=hbase-0.98.24
HBASE_ARCHIVE=$HBASE_VERSION-hadoop2-bin.tar.gz
HBASE_MIRROR_DOWNLOAD=http://apache.volia.net/hbase/0.98.24/$HBASE_ARCHIVE
HBASE_RES_DIR=/vagrant/resources/hbase
HBASE_CONF_DIR=/usr/local/hbase/conf

# ssh
SSH_RES_DIR=/vagrant/resources/ssh
RES_SSH_COPYID_ORIGINAL=$SSH_RES_DIR/ssh-copy-id.original
RES_SSH_COPYID_MODIFIED=$SSH_RES_DIR/ssh-copy-id.modified
RES_SSH_CONFIG=$SSH_RES_DIR/config

function resourceExists {
	FILE=/vagrant/resources/$1
	if [ -e $FILE ]
	then
		return 0
	else
		return 1
	fi
}

function fileExists {
	FILE=$1
	if [ -e $FILE ]
	then
		return 0
	else
		return 1
	fi
}
