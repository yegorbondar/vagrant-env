#!/bin/bash

source "/vagrant/scripts/common.sh"

function installLocalHBase {
	echo "install hbase from local file"
	FILE=/vagrant/resources/$HBASE_ARCHIVE
	tar -xzf $FILE -C /usr/local
}

function installRemoteHBase {
	echo "install hbase from remote file"
	curl -sS -o /vagrant/resources/$HBASE_ARCHIVE -O -L $HBASE_MIRROR_DOWNLOAD
	tar -xzf /vagrant/resources/$HBASE_ARCHIVE -C /usr/local
}

function installHBase {
	if resourceExists $HBASE_ARCHIVE; then
		installLocalHBase
	else
		installRemoteHBase
	fi
	ln -s /usr/local/$HBASE_VERSION-hadoop2 /usr/local/hbase
	mkdir /usr/local/hbase/logs /usr/local/hbase/derby/
}

function setupHBase {
	echo "copying over hbase configuration file"
	cp -f $HBASE_RES_DIR/* $HBASE_CONF_DIR
}

function setupEnvVars {
	echo "creating hbase environment variables"
	cp -f $HBASE_RES_DIR/hbase.sh /etc/profile.d/hbase.sh
	. /etc/profile.d/hbase.sh
}

function runHBaseServices {
	echo "starting hbase"
	/usr/local/hbase/bin/start-hbase.sh
}

echo "setup hbase"

installHBase
setupHBase
setupEnvVars
runHBaseServices

echo "hbase setup complete"
