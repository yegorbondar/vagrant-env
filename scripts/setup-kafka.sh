#!/bin/bash

source "/vagrant/scripts/common.sh"

function installLocalKafka {
	echo "install kafka from local file"
	FILE=/vagrant/resources/$KAFKA_ARCHIVE
	tar -xzf $FILE -C /usr/local
}

function installRemoteKafka {
	echo "install kafka from remote file"
	curl -sS -o /vagrant/resources/$KAFKA_ARCHIVE -O -L $KAFKA_MIRROR_DOWNLOAD
	tar -xzf /vagrant/resources/$KAFKA_ARCHIVE -C /usr/local
}

function setupKafka {
	echo "setup kafka"
}

function setupEnvVars {
	echo "creating kafka environment variables"
	cp -f $KAFKA_RES_DIR/kafka.sh /etc/profile.d/kafka.sh
	. /etc/profile.d/kafka.sh
}

function installKafka {
	if resourceExists $KAFKA_ARCHIVE; then
		installLocalKafka
	else
		installRemoteKafka
	fi
	ln -s /usr/local/$KAFKA_VERSION /usr/local/kafka
	mkdir -p /usr/local/kafka/logs/history
}

function startServices {
	echo "starting Kafka"
	nohup /usr/local/kafka/bin/kafka-server-start.sh /usr/local/kafka/config/server.properties >  /usr/local/kafka/logs/kafka.log 2>&1 &
}

echo "setup kafka"

installKafka
setupKafka
setupEnvVars
startServices

echo "kafka setup complete"
