#!/bin/bash

source "/vagrant/scripts/common.sh"

function installLocalMongo {
	echo "install mongo from local file"
	FILE=/vagrant/resources/$MONGODB_ARCHIVE
	tar -xzf $FILE -C /usr/local
}

function installRemoteMongo {
	echo "install mongo from remote file"
	curl -sS -o /vagrant/resources/$MONGODB_ARCHIVE -O -L $MONGODB_MIRROR_DOWNLOAD
	tar -xzf /vagrant/resources/$MONGODB_ARCHIVE -C /usr/local
}

function setupMongo {
	echo "setup mongo"
	mkdir -p /home/vagrant/data/db
}

function setupEnvVars {
	echo "creating mongo environment variables"
	cp -f $MONGODB_RES_DIR/mongodb.sh /etc/profile.d/mongodb.sh
	. /etc/profile.d/mongodb.sh
}

function installMongo {
	if resourceExists $MONGODB_ARCHIVE; then
		installLocalMongo
	else
		installRemoteMongo
	fi
	ln -s /usr/local/$MONGODB_VERSION /usr/local/mongodb
	mkdir -p /usr/local/mongodb/logs/history
}

function startServices {
	echo "starting Mongo"
	mongod --fork --logpath /usr/local/mongodb/logs/history --dbpath /home/vagrant/data/db
}

echo "setup mongo"

installMongo
setupMongo
setupEnvVars
startServices

echo "mongo setup complete"
