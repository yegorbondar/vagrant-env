#!/bin/bash

source "/vagrant/scripts/common.sh"

function startDaemons {
    echo "starting zookeeper"
    nohup /usr/local/zookeeper/bin/zkServer.sh start >  /usr/local/zookeeper/logs/zookeeper.log 2>&1 &
    sleep 20
    echo "starting Kafka"
    nohup /usr/local/kafka/bin/kafka-server-start.sh /usr/local/kafka/config/server.properties >  /usr/local/kafka/logs/kafka.log 2>&1 &
    sleep 10
    echo "starting Spark services"
    /usr/local/spark/sbin/start-master.sh
    /usr/local/spark/sbin/start-slave.sh spark://10.211.55.101:7077
    /usr/local/spark/sbin/start-history-server.sh
    sleep 10
    echo "starting hbase"
    /usr/local/hbase/bin/start-hbase.sh
}

startDaemons
